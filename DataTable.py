class DataTable:
    def __init__(self, path_file_csv) -> None:
        self.data = self.get_data_in_file(path_file_csv)
    
    def get_data_in_file(self, path_file):
        arr = []
        with open(path_file) as f:
            for line in f:
                items = line[:-1].split(',')
                arr.append(items)
            
        return arr
    
    def print_value(self):
        for row in self.data:
            item_row = ""
            for col in row:
                item_row += f"{col}\t"
            print(item_row)
    
if __name__ == "__main__":
    print("Start")
    datatable = DataTable('./Datas/DataTree.csv')
    datatable.print_value()