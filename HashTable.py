class HashTable:
    def __init__(self, LengthMax = 10) -> None:
        self.length = LengthMax
        self.arr = [[] for i in range(self.length)]

    def get_hash(self, key):
        total = 0
        for char in key:
            total += ord(char)
        return total % self.length
    
    def add(self, key, value):
        index = self.get_hash(key)
        for key_loop, value_loop in self.arr[index]:
            if key_loop == key:
                return          
        self.arr[index].append((key, value))
        
    def delete(self, key):
        index = self.get_hash(key)
        values = self.arr[index]
        for index_item, element_item in enumerate(values):
            if key == element_item[0]:
                del values[index_item]
        
    def get_value(self, key):
        index = self.get_hash(key)
        result = ""
        for key_value, value in self.arr[index]:
            if key == key_value:
                result = f"key: {key_value} - value: {value}"
                break
        return result

    def print_list(self):
        result = ""
        for index, values in enumerate(self.arr):
            result_value = ""
            for key, value in values:
                result_value += f"key: {key} - value: {value}, "
            result += f"index: {index} - values: [{result_value[:-2]}]\n"
        print("Print list hash table:\n" + result)

    def __getitem__(self, key):
        return self.get_value(key)
    
    def __setitem__(self, key, value):
        self.add(key, value)

    def __delitem__(self, key):
        self.delete(key)








if (__name__ == "__main__"):
    print("Start")
    names = HashTable()
    names["march 6"] = 2350
    names["march 7"] = 2365
    names["march 8"] = 8456
    names.add("march 17", 1620)
    names.add("march 18", 6352)
    names.add("march 19", 2568)
    names.print_list()
    del names["march 6"]
    names.print_list()
    
    