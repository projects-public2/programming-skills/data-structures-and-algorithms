class Stack:
    def __init__(self) -> None:
        self.arr = []
        pass
    
    def push(self, value):
        self.arr.append(value)
        pass
    
    def pop(self):
        if len(self.arr) == 0:
            return None
        
        item_remove = self.arr[-1]
        self.arr.remove(item_remove)
        return item_remove
    
    def contain(self, value) -> bool:
        for item in self.arr:
            if item == value:
                return True
        return False
    
    def print_item(self):
        result = ""
        for index, value in enumerate(self.arr):
            result += f"Index: {index} - Value: {value}\n"
        print(result)


if __name__ == "__main__":
    print("Start")
    stack = Stack()
    stack.push(1)
    stack.push(2)
    stack.push(3)
    stack.push(4)
    stack.push(5)
    stack.print_item()
    print(f"Item 5 is contain: {stack.contain(5)}")
    print(f"Item remove: {stack.pop()}")
    stack.print_item()