class TreeNode:
    def __init__(self, data) -> None:
        self.data = data
        self.parent = None
        self.childrens = []
        
    def add_child(self, node_children):
        node_children.parent = self
        self.childrens.append(node_children)
        
    def get_level(self) -> int:
        level = 0
        p = self.parent
        while(p != None):
            level += 1
            p = p.parent
            
        return level
    
    def print_tree(self):
        level = self.get_level()
        space_prefix = ' ' * (level) * 3
        print(f"{space_prefix}{self.data}")
        if self.childrens:
            for child in self.childrens:
                child.print_tree()
                

def build_product_tree() -> TreeNode:
    root = TreeNode("Electronics")
    
    laptop = TreeNode("Laptop")
    laptop.add_child(TreeNode("Mac"))
    laptop.add_child(TreeNode("Surface"))
    laptop.add_child(TreeNode("Thinkpad"))
    
    cell_phone = TreeNode("Cell Phone")
    cell_phone.add_child(TreeNode("iPhone"))
    cell_phone.add_child(TreeNode("Google Pixel"))
    cell_phone.add_child(TreeNode("Vivo"))
    
    tv = TreeNode("TV")
    tv.add_child(TreeNode("Samsung"))
    tv.add_child(TreeNode("LG"))
    
    root.add_child(laptop)
    root.add_child(cell_phone)
    root.add_child(tv)
    return root

def build_tree_from_file() -> TreeNode:
    root: TreeNode = None
    with open('./Datas/DataTree.csv', 'r') as f:
        for line in f:
            data_parent, data_current = tuple(line[:-1].split(','))
            if data_parent == "ï»¿":
                root = TreeNode(data_current)
            else:
                node_finded = find_node(root, data_parent)
                node_finded.add_child(TreeNode(data_current))
    return root
         
def find_node(root: TreeNode, data_need_find) -> TreeNode | None:
    if root == None:
        return None
    
    if root.data == data_need_find:
        return root
    
    for node_child in root.childrens:
        if node_child.data == data_need_find:
            return node_child
        find_node(node_child, data_need_find)
    return None
        


if __name__ == "__main__":
    print("Start")
    tree = build_tree_from_file()
    tree.print_tree()
    
    