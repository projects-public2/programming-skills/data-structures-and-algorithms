from functools import wraps
import inspect
from typing import Callable, Generic, Iterable, Self, Type, TypeVar, Union, overload

T = TypeVar("T")

def check_function_callback(func):
    @wraps(func)
    def check(self, callback, *args):
        if not (inspect.isfunction(callback)): return False # raise Exception("Tham số {} không phải là function".format(func.__name__))
        if (callback.__code__.co_argcount < 2): return False # raise Exception("Function {} phải có ít nhất là 2 tham số".format(func.__name__))       
        return func(self, callback, *args)
    
    return check
        

class Node:
    def __init__(self, data, next = None) -> None:
        self.Data = data
        self.Next = next
        
    
class LinkedList(Generic[T]):
    def __init__(self) -> None:
        self.Head: Node = None
        self.Last: Node = None
        self.Lenth = 0
    
    def add(self, data: T) -> bool:
        node = Node(data, self.Head)
        self.Head = node
        
        if self.Last == None:       # Element first => Head và Last trỏ chung 1 giá trị
            self.Last = node
        self.Lenth += 1
        
        return True
    
    def adds(self, data: Iterable[T]) -> bool:
        if len(data) == 0: return False            
        
        for item in data:
            if not self.add(item): return False
        
        return True

    def insert_at(self, index: int, data: T) -> bool:
        if (index < 0 or index >= self.Lenth):
            return False
        
        if (index == 0):
            if not (self.add(data)): return False
            return True
        
        if (index == self.Lenth - 1):
            if not (self.append(data)): return False
            return True
        
        index_loop = 0
        current_node = self.Head
        while(current_node):
            if (index_loop == index - 1):
                node = Node(data, current_node.Next)
                current_node.Next = node
                break
                
            current_node = current_node.Next
            index_loop += 1
        
        self.Lenth += 1
        
    def inserts_at(self, index: int, datas: Iterable[T]) -> bool:
        if (index < 0 or index > self.Lenth):
            return False
        
        if (index == 0):
            if not (self.appends(datas)): return False
            return True
        
        if (index == self.Lenth):
            if not (self.append(datas)): return False
            return True
        
        if (len(datas) == 0):
            return False
        
        data_temps = LinkedList[T]()
        data_temps.appends(datas)
        
        index_loop = 0
        current_node = self.Head
        while(current_node):
            if (index_loop == index - 1):
                data_temps.Last.Next = current_node.Next
                current_node.Next = data_temps.Head
                break
                
            current_node = current_node.Next
            index_loop += 1
        
        self.Lenth += len(datas)

    def append(self, data: T) -> bool:
        if self.Last == None:
            self.add(data)
            return True
        
        node = Node(data)
        self.Last.Next = node
        self.Last = node
        self.Lenth += 1
        return True

    def appends(self, iterable: Iterable[T]) -> bool:
        if len(iterable) == 0: return False
        
        for item in iterable:
            self.append(item)
            
        return True
    
    def remove(self, data: T) -> bool:
        if (self.Head == None): return False
        
        if (self.Head.Data == data):
            self.Head = self.Head.Next
            self.Lenth -= 1
            return True
        
        current_node = self.Head
        while(current_node):
            if (current_node.Next.Data == data):
                current_node.Next = current_node.Next.Next
                self.Lenth -= 1
                return True
            current_node = current_node.Next

        return True
    
    def remove_all_value(self, data: T) -> bool:
        if (self.Lenth == 0): return False
        if (self.Head == None): return False
        
        prev_node = None
        current_node = self.Head
        while(current_node):
            if (current_node == self.Head) and (current_node.Data == data):
                node_remove = current_node
                current_node = current_node.Next
                node_remove.Next = None
                self.Head = current_node
                self.Lenth -= 1
                continue
            
            if (current_node == self.Last) and (current_node.Data == data):
                prev_node.Next = None
                self.Last = prev_node
                self.Lenth -= 1
                break
            
            if (current_node.Data == data):
                node_remove = current_node
                current_node = current_node.Next
                prev_node.Next = current_node
                node_remove.Next = None
                self.Lenth -= 1
            else:
                if (current_node == self.Last):
                    node_remove = None
                    break
                prev_node = current_node
                current_node = current_node.Next
        return True
    
    def remove_all(self) -> bool:
        if (self.Lenth == 0): return False
        
        current_node = self.Head
        while(current_node):
            node_remove = current_node
            current_node = current_node.Next
            node_remove.Next = None
            
        self.Head = None
        self.Last = None
        self.Lenth = 0            
        return True
    
    def remove_at(self, index: int) -> bool:
        if (self.Head == None): return False
        if (index < 0 or index >= self.Lenth): return False
        
        if (index == 0):
            node_next = self.Head.Next
            self.Head.Next = None
            self.Head = node_next
            self.Lenth -= 1
            return True
        
        if (index == self.Lenth - 1):
            current_node.Next = None
            self.Last = current_node
            self.Lenth -= 1
            return True
              
        index_loop = 0
        current_node = self.Head
        while(current_node):
            if (index_loop == index - 1):
                if current_node.Next == self.Last:
                    current_node.Next = None
                    self.Last = current_node
                else:
                    node_next = current_node.Next.Next
                    current_node.Next.Next = None
                    current_node.Next = node_next
                break
                
            current_node = current_node.Next
            index_loop += 1
        
        self.Lenth -= 1
        return True
    
    def removes_at(self, index_start: int, index_end: int) -> bool:
        if (self.Head == None): return False
        if (index_start < 0 or index_start >= self.Lenth): return False        
        if (index_end < 0 or index_end >= self.Lenth): return False        
        if (index_start > index_end): return False
        
        for number_of_repetitions in range(0, index_end - index_start + 1):
            self.remove_at(index_start)
            
        return True
    
    def find_index(self, data: T) -> int:
        if self.Head == None: return -1
        index_loop = 0
        current_node = self.Head
        while(current_node):
            if (current_node.Data == data):
                return index_loop
            current_node = current_node.Next
            index_loop += 1
        return -1
    
    @check_function_callback
    def where(self, if_callback, *args) -> Self:
        if self.Lenth == 0: return Self            
        
        result = LinkedList[T]()
        index_loop = 0
        current_node = self.Head
        
        try:
            while current_node:
                    if if_callback(index_loop, current_node.Data, *args):
                        result.append(current_node.Data)
                    current_node = current_node.Next
                    index_loop += 1
        except Exception as ex:
            raise Exception(ex)
                    
        return result
    
    @check_function_callback
    def foreach(self, action_callback, *args) -> Self:
        if self.Lenth == 0: return Self   
        
        index_loop = 0
        current_node = self.Head
        try:
            while(current_node):
                    action_callback(index_loop, current_node.Data, *args)
                    current_node = current_node.Next
                    index_loop += 1
        except Exception as ex:
            raise Exception(ex)
        
    def print_list(self) -> None:
        node_item = self.Head
        result = ''
        while(node_item):
            result += str(node_item.Data) + ', '
            node_item = node_item.Next
        print(result[:-2])




if __name__ == "__main__":
    print("Start")
    numbers = LinkedList[int]()
    
    # Cách sử dụng
    numbers.add(1)
    numbers.add(2)
    numbers.print_list()
    print("Length list add: ", numbers.Lenth)
    
    numbers.adds([3, 4, 5, 6])
    numbers.print_list()
    print("Length list adds: ", numbers.Lenth)
    
    numbers.append(7)
    numbers.print_list()
    print("Length list append: ", numbers.Lenth)
    
    numbers.appends([8, 9, 10])
    numbers.print_list()
    print("Length list appends: ", numbers.Lenth)
    
    numbers.insert_at(3, 11)
    numbers.print_list()
    print("Length list insert at: ", numbers.Lenth)
    
    numbers.inserts_at(6, [12, 13, 14])
    numbers.print_list()
    print("Length list inserts at: ", numbers.Lenth)
    
    numbers.remove(3)
    numbers.print_list()
    print("Length list remove: ", numbers.Lenth)
    
    numbers.remove_at(8)
    numbers.print_list()
    print("Length list remove at: ", numbers.Lenth)
    
    numbers.removes_at(2, 5)
    numbers.print_list()
    print("Length list removes at: ", numbers.Lenth)
    
    numbers.appends([6, 7, 8, 6, 10, 13, 6])
    numbers.print_list()
    print("Length list appends: ", numbers.Lenth)
    
    numbers.remove_all_value(6)
    numbers.print_list()
    print("Length list removes at: ", numbers.Lenth)
    
    numbers_where = numbers.where(lambda index, data, item_divisible: (data % item_divisible == 0), 2)
    numbers_where.print_list()
    print("Length list where: ", numbers_where.Lenth)
    
    numbers.where(lambda index, data, item_divisible: (data % item_divisible == 0), 2).foreach(lambda index, data: print("index {} - data {}".format(index, data)))
    print("Length list foreach: ", numbers.Lenth)
    