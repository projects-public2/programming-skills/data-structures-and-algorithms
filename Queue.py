class Queue:
    def __init__(self):
        self.arr = []
    
    def push(self, value):
        self.arr.insert(0, value)
    
    def pop(self):
        if (len(self.arr) == 0):
            return None        
        return self.arr.pop()
    
    def print_item(self):
        print("List queue: ")
        for index, item in enumerate(self.arr):
            print(f"Index: {index} - value: {item}")
    

if __name__ == "__main__":
    print("Start")
    queue = Queue()
    queue.push({
        "company": "Wal Mart",
        "timestamp": "15 apr, 11.01 AM",
        "price": 131.10
    })
    queue.push({
        "company": "Wal Mart",
        "timestamp": "15 apr, 11.02 AM",
        "price": 131.35
    })
    queue.push({
        "company": "Wal Mart",
        "timestamp": "15 apr, 11.03 AM",
        "price": 131.15
    })
    queue.push({
        "company": "Wal Mart",
        "timestamp": "15 apr, 11.04 AM",
        "price": 133.25
    })
    queue.push({
        "company": "Wal Mart",
        "timestamp": "15 apr, 11.05 AM",
        "price": 132.10
    })
    queue.print_item()
    print("item remove: ", queue.pop())
    queue.print_item()
